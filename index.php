<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Status check for Wikimedia Ukraine blog posts links</title>
        <link rel="stylesheet" type="text/css" href="styles.css" />
    </head>
    <body>
        <table>
            <tr>
                <th>URL (decoded)</th>
                <th>Status</th>
            </tr>
            <?php
            $testing = true;
            error_reporting(!(E_USER_NOTICE & E_WARNING & E_DEPRECATED));
            $posts = [];
            fetch_posts("wikimediaukraine.wordpress.com");

            // Well, I prefer 0-indexed but this way is better for
            // "N out of M" sentences
            $post_index = 1;

            // Iterating posts
            foreach ($posts as $post) {
                echo "<tr>";
                echo "<td class=\"postname\" colspan=\"2\">";
                $post_url = $post->URL;
                $post_title = $post->title;
                echo "<a href=\"$post_url\">$post_title</a> (#$post_index/$posts_found)";
                echo "</td>";
                echo "</tr>";
                $content = $post->content;
                $post_dom = new DomDocument();
                // TODO Needs to be checked if really does something:
                $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
                $post_dom->loadHTML($content);
                $a_elements = $post_dom->getElementsByTagName("a");
                $a_elements_length = $a_elements->length;

                //TODO move to method
                for ($i = 0; $i < $a_elements_length; $i++) {
                    $href_object = new href();
                    $wiki_object = new wiki();

                    // Reading the HTML element URL attribute to work on:
                    // TODO implement reading of IMG->src attribute here as well
                    $url = $a_elements->item($i)->attributes->getNamedItem("href")->nodeValue;
                    proceed_link($url);
                }

                $post_index++;
                if ($testing) {
                    break;
                }
            }// end post iterator
//var_dump($posts->$content);

            /**
             * Fetches blog posts
             * @global type $posts
             * @param type $blog -- URL of the wordpress.com blog
             * 
             */
            function fetch_posts($blog) {
                global $posts, $posts_found;
                $fetch_number = 0;
                do {
                    $posts_url = "https://public-api.wordpress.com/rest/v1.1/sites/$blog/posts/?number=100&offset=$fetch_number";
                    //var_dump($posts_url);
                    $posts_json = file_get_contents($posts_url);
                    $posts_json_decode = json_decode($posts_json);
                    $posts = array_merge($posts, $posts_json_decode->posts);
                    //var_dump($posts);
                    $posts_now = count($posts);
                    $fetch_number = $posts_now;

                    $posts_found = $posts_json_decode->found;
//                var_dump($posts_now);
//                var_dump($posts_found);
                    if ($posts_now < $posts_found) {
                        $fetchmore = true;
                    } else {
                        $fetchmore = false;
                    }
                    if ($testing) {
                        break;
                    }
                } while ($fetchmore);
            }

            /**
             * Checks site from $href.
             * Writes HTTP status or curl error number into "status" field.
             * @global type $href
             * 
             */
            function check_site() {
                global $href_object;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $href_object->url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // to remove CURLE_SSL_CACERT error

                $output = curl_exec($ch);
                if (!curl_errno($ch)) {
                    $href_object->status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                } else {
                    $href_object->status = "Failed to load: " . curl_errno($ch);
                }
                curl_close($ch);
            }

            /**
             * Checks if page exists in the given wiki
             * @global type $href
             * @global type $wiki_page_exists
             * 
             */
            function check_wiki() {
                global $href_object, $wiki_object;
                if ($href_object->has_page) {
                    $wiki_object->page = $href_object->path_split[2];
                    $wiki_object->query = "https://{$href_object->host}/w/api.php?action=query"
                            . "&format=json&prop=info&titles={$wiki_object->page}&redirects=1";
                } elseif ($href_object->has_title) {
                    return;
                } elseif ($href_object->has_oldid) {
                    return;
                } else {
                    return;
                }
                //var_dump($wiki_query);

                $wiki_object->query_json = file_get_contents($wiki_object->query);
                //var_dump($wiki_query_json);
                $wiki_object->query_json_decode = json_decode($wiki_object->query_json);
                $wiki_object->query_pages = $wiki_object->query_json_decode->query->pages;
                $wiki_object->page_exists = false;
                foreach ($wiki_object->query_pages as $key => $value) {
                    //var_dump($key);
                    $wiki_object->page_exists = $key == -1 ? false : true;
                }
            }

            /**
             * 
             *  Outputs table row with link and its status.
             *  Does nothing in case of successful status
             *  and <tt>ignoresuccess</tt> param was provided
             * 
             * @global type $href
             * @global type $wiki_page_exists
             * @global type $decoded_href
             * @global type $wiki_page
             */
            function output_link() {
                global $href_object, $wiki_object, $decoded_href;
                if (!($_REQUEST["ignoresuccess"] && ($href_object->status == 200 | $wiki_object->page_exists === true))) {
                    echo "<tr>";

                    //URL
                    echo "<td>";
                    echo $decoded_href;
                    echo "</td>";

                    echo "<td>";
                    if (isset($href_object->status)) {
                        echo "HTTP: {$href_object->status}";
                    }
                    if (isset($wiki_object->page)) {
                        echo "Wiki&nbsp;page: " . ($wiki_object->page_exists ? "blue" : "red");
                    }
                    echo "</td>";

                    //var_dump($href_object);

                    echo "</tr>";
                }
            }

            /**
             * Is supposed to represent the URL we are dealing with
             */
            class href {

                public $url;
                public $host;
                public $path;
                public $path_split;
                public $query;
                public $query_part;
                public $query_part_split;
                public $has_page;
                public $has_curid;
                public $has_title;
                public $status;
                public $is_a_wiki;

            }

            /**
             * Is supposed to represent wiki page we are dealing with
             */
            class wiki {

                public $page;
                public $page_exists;
                public $query;
                public $query_json;
                public $query_json_decode;
                public $query_pages;

            }

            function proceed_link($url) {
                global $href_object, $wiki_object;
                $href_object = new href();
                $wiki_object = new wiki();
                // Reading the HTML element URL attribute to work on:
                // TODO implement reading of IMG->src attribute here as well
                $href_object->url = $url;
                // URLdecoding URL for more attractive output:
                decompose_url();
                // TODO read list of wikimedia wikis from somewhere instead
                // of using hardcoded values
                $href_object->is_a_wiki = (
                        $href_object->host == "uk.wikipedia.org" || $href_object->host == "commons.wikimedia.org" || $href_object->host == "ua.wikimedia.org"
                        ) &&
                        ($href_object->has_page || $href_object->has_curid || $href_object->has_title );
                if ($href_object->is_a_wiki) {
                    check_wiki();
                } else {
                    check_site();
                }
                // Outputs table row with link and its status.
                // Does nothing in case of successful status and param to skip such provided
                output_link();
                unset($href_object);
            }

            function decompose_url() {
                global $decoded_href, $href_object;
                $decoded_href = urldecode($href_object->url);
                // parsing URL for further analysis:
                $parsed_href = parse_url($href_object->url); //would use to determine if a Wikimedia wiki

                $href_object->host = $parsed_href["host"];
                $href_object->path = $parsed_href["path"];

                $href_object->path_split = preg_split("/\//", $href_object->path);

                $href_object->has_page = $href_object->path_split[1] == "wiki" && $href_object->path_split[2];
                $href_object->query = $parsed_href["query"];
                $href_object->query_split = preg_split("/&/", $href_object->query);
                $href_object->has_curid = false;
                foreach ($href_object->query_split as $href_object->query_part) {
                    $href_object->query_part_split = preg_split("/=/", $href_object->query_part);
                    if ($href_object->query_part_split[0] == "curid") {
                        $href_object->has_curid = true;
                    }
                    if ($href_object->query_part_split[0] == "title") {
                        $href_object->has_title = true;
                    }
                }
            }

            /**
             * From http://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
             * @param type $url
             * @param type $data
             * @return type
             * 
             */
            function httpPost($url, $data) {
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($curl);
                curl_close($curl);
                return $response;
            }
            ?>
        </table>
    </body>
</html>
